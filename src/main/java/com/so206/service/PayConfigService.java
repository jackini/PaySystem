package com.so206.service;

import com.so206.po.SystemPayConfigWithBLOBs;


public interface PayConfigService {

    SystemPayConfigWithBLOBs getPayConfig(Integer id);

    void updatePPayConfig(SystemPayConfigWithBLOBs payConfig);
}
